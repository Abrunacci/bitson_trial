from trytond.pool import Pool
from .bitson import *


def register():
    Pool.register(Bitson,module='bitson',type_='model')

