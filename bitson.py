from trytond.model import ModelView, ModelSQL, Workflow, fields
from trytond.pyson import If, Eval, Bool, Id
from trytond.pool import Pool
import datetime
import ipdb

__all__ = ['Bitson']

class Bitson(Workflow, ModelSQL, ModelView):
    'Members'
    __name__ = 'bitson.members'
    name = fields.Char('Name', required=True)
    birthday = fields.Date('Birthday')
    age = fields.Function(fields.Integer('age', readonly=True), 'on_change_with_age')
    party = fields.Many2One('party.party', 'Party', required=True)
    email = fields.Char('Mail')
    state = fields.Selection([('draft','Draft'),
                              ('added','Added'),
                              ('sended','Sended'),
                              ],'State',readonly=True)


    @staticmethod
    def default_state():
        return 'draft'


    @fields.depends('birthday')
    def on_change_with_age(self, name=None):
        if self.birthday:
            age = datetime.datetime.now().year - self.birthday.year
            return int(age)
        return None


    @classmethod
    def __setup__(cls):
        super(Bitson, cls).__setup__()
        cls._transitions |= set ((
            ('draft','added'),
            ('added','sended'),
            ))
        cls._buttons.update({
            'sendmail': {'invisible': Eval('state') == 'draft',
                         'readonly': Eval('state') == 'sended'},
            'save': {'readonly': Eval('state') != 'draft' },
            })


    @classmethod
    @ModelView.button
    @Workflow.transition('added')
    def save(cls, bitsons):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('sended')
    def sendmail(cls, bitsons):
        for b in bitsons:
            print b.email
        pass
